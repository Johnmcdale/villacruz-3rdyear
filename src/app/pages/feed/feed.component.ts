import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from '../../chat.service';
import { startWith } from 'rxjs/operators';
import { interval } from 'rxjs';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit {

  username: string;
  message: string;
  feeds: any;
  intervalObj: any;
  constructor(public route: Router, public chatSvc: ChatService) {
    if (localStorage.getItem('username') === null) {
      this.route.navigateByUrl('/login');
    }

    this.username = localStorage.getItem('username');
  }

  postFeed() {
    this.chatSvc.postFeed(this.username, this.message)
    .subscribe(resp => {
      this.getFeed();
    });
  }

  getFeed() {
    this.chatSvc.getFeed().subscribe(resp => {
      this.feeds = resp;
      this.feeds.reverse();
    });
  }

  ngOnInit() {
    this.intervalObj = interval(3000).pipe(startWith(0))
    .subscribe(() => {
      this.getFeed();
    });
    this.getFeed();
  }

}
